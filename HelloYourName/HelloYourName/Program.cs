﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloYourName
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "Damian";
            var greeting = $"Hello {myName}, Hope you have a great day!";
            Console.WriteLine(greeting);
        }
    }
}
